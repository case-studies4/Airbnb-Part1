# Willkommen bei [Airbnb Dashboard Projekt Part1] 

Dieses Projekt verwendet Daten von [Airbnb](http://insideairbnb.com/get-the-data/), um ein Dashboard mit verschiedenen Statistiken zu erstellen. 

## Über das Projekt

- **Avg Price Per Bedroom:** Durchschnittspreis pro Schlafzimmer.
    Dieses Diagramm zeigt den durchschnittlichen Mietpreis pro Nacht in Abhängigkeit von der Anzahl der Schlafzimmer in den Airbnb-Unterkünften.
    Es ermöglicht einen schnellen Überblick über die durchschnittlichen Kosten in Bezug auf die Schlafzimmeranzahl.
- **Price Per Zipcode:** Preis pro Postleitzahl.
    Hier wird eine geografische Darstellung der Mietpreise für Airbnb-Unterkünfte anhand der Postleitzahlen präsentiert.
    Das Diagramm ermöglicht es, Regionen mit höheren oder niedrigeren Durchschnittspreisen auf einen Blick zu erkennen.
- **Revenue for Year:** Umsatz für das Jahr.
    Dieses Diagramm veranschaulicht den Gesamtumsatz, den Airbnb-Unterkünfte im Laufe des Jahres generieren.
    Es gibt einen Einblick in saisonale Schwankungen oder Trends und zeigt, in welchen Monaten der Umsatz besonders hoch oder niedrig ist.
- **Distinguishing Count of Bedroom Listings:** Unterscheidende Anzahl der Schlafzimmer-Listings.   
    Dieses Diagramm gibt eine differenzierte Aufschlüsselung der Anzahl der Unterkünfte basierend auf der Anzahl der Schlafzimmer. Es ermöglicht, die Verteilung der Airbnb-Listings nach der Anzahl der Schlafzimmer zu verstehen und eventuell herauszufinden, welche Schlafzimmer-Konfigurationen am häufigsten oder am seltensten sind.

## Tableau 

Besuche [Dashboard](https://public.tableau.com/app/profile/faezeh4551/viz/AirBnBPart1Project/Dashboard2?publish=yes), um die erstellten Statistiken anzuzeigen.



